import {
    html,
    css,
    LitElement
} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

export class ShowPrice extends LitElement {
	static properties = {
		code: {type: String},
		name: {type: String},
		price: {type: Number},
	}

	constructor() {
		super();
	}

	render() {
		return html`
		<h2>[${this.code}] ${this.name}  ==> ${this.price}$</h2>
		`;
	}
}



customElements.define('ceco-show-price', ShowPrice)