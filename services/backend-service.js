const { timer } = rxjs;
const { 
	tap,
	takeUntil,
	take,
	map,
	combineLatestWith,
	distinctUntilChanged,
	debounceTime,
} = rxjs.operators;



export class BackendService {

    searchCounter = 0;
    urlServer = '';
    lastSearches = [];


    constructor(urlServer) {
        this.urlServer = urlServer;
    }

    searchPrice$(code) {
        /** Incrementamos el contador de búsquedas */
        this.searchCounter = this.searchCounter + 1;
        /** Buscar la data en el servidor */
        // fetch(this.urlServer)  <<<<--- Propiedad del objeto
        const response$ = timer(3000)
            .pipe(
                map( () => ({name: 'Harina otra mentira', code: code, price: 3.5})),
                tap( search => this.lastSearches = [search].concat(this.lastSearches)),
                tap( () => console.log('Que hay en this.lastSearches:', this.lastSearches))
            )
            return response$
    }

    getSearchCounter() {
        return this.searchCounter;
    }

    getUrlServer() {
        return this.urlServer;
    }

    getLastSearches() {
        return this.lastSearches;
    }

}


/*


*/