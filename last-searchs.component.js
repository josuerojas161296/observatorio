import {
    html,
    css,
    LitElement
} from 'https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js';

export class LastSearchs extends LitElement {
	static properties = {
		prices: {type: Array}
	}

	constructor() {
		super();
	}

	render() {
		return html`
		<h2>Penúltimas Consultas</h2>
		<div>(sin incluir la última, que estás viendo en la linea de arriba :( )</div>
		${this.prices.map( (item) => html`
			<ceco-show-price 
				.code=${item.code}
				.name=${item.name}
				.price=${item.price}			
			></ceco-show-price`
		)}
		`;
	}
}



customElements.define('ceco-last-searchs', LastSearchs)