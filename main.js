import {
  html,
  css,
  LitElement,
} from "https://cdn.jsdelivr.net/gh/lit/dist@2/all/lit-all.min.js";

const { interval } = rxjs;
const { tap, take, catchError, switchMap, map } = rxjs.operators;

import { BackendService } from "./services/backend-service.js";

import { PoteService } from "./services/pote-service.js";

export class MainComponent extends LitElement {
  /**
   * Propiedad que contiene el último producto consultado
   */
  lastProduct = { code: "", name: "", price: 0 };

  message = "";

  constructor() {
    super();
    /** Creamos una instancia del servicio del backend */
    this.backendSrv = new BackendService("https://kana.server/graphql");
    /** Inicializamos la conexión con el servicio de backend de Pote */
    this.poteSrv = new PoteService();
  }

  render() {
    return html`
      <!--h1>Consultor de precios</h1>
        <ceco-code-writer @searchPrice=${this.searchPrice}></ceco-code-writer>
        <ceco-show-price .code="${this.lastProduct.code}" .name="${this
        .lastProduct.name}" .price="${this.lastProduct
        .price}"></ceco-show-price>
        <hr/>
        <ceco-last-searchs .prices=${this.backendSrv.getLastSearches()}></ceco-last-searchs>
        <small>Haz realizado ${this.backendSrv.getSearchCounter()} en ${this.backendSrv.getUrlServer()} búsquedas</small-->
      <div>
        <h2>Pruebas para PoteSrv</h2>
        <div>
          <button @click=${this.findPrice}>Buscar precio</button>
        </div>

        <div>
          <button @click=${this.echo}>Echo</button>
        </div>
        <div>
          <button @click=${this.login}>Login</button>
        </div>
        <div>
          <button @click=${this.configuration}>Establecer configuración</button>
        </div>
        <div>
          <button @click=${this.openPos}>Abrir Caja</button>
        </div>
        <div>
          <button @click=${this.createInvoice}>Crear factura</button>
        </div>

        <div style="margin-top: 20px">
          <button @click=${this.createInvoiceShortcut}>
            Crear factura Atajo
          </button>
        </div>
        <div>
          <button @click=${this.addItem}>Agregar producto</button>
        </div>
        <div>
          <button @click=${this.removeItem}>Eliminar Producto</button>
        </div>
        <div>
          <button @click=${this.saveDraftInvoice}>Guardar en borrador</button>
        </div>
        <div>
          <button @click=${this.recoverDraftInvoice}>
            Recuperar factura 1
          </button>
        </div>
        <button @click=${this.clearInvoice}>Limpiar Factura</button>
        </div>
        <div style="margin-top: 20px">
          <button @click=${this.makePaymentVed}>Hacer un pago de 20 Bs</button>
        </div>
        <div style="margin-top: 20px">
          <button @click=${this.makePaymentUsd}>Hacer un pago de 5$</button>
        </div>

        <div style="margin-top: 20px">
          mensaje última operación:
          <div>${this.message}</div>
        </div>
      </div>
    `;
  }

  searchPrice(event) {
    console.log("MAIN::::: Buscando guayaba:", event);
    /** Extraemos del código del evento */
    const code = event.detail.code;
    /** Llamamos al servicio para buscar precio */
    const backend$ = this.backendSrv.searchPrice$(code).pipe(
      tap((info) => console.log("GOTICA desde backend:", info)),
      tap((price) => (this.lastProduct = price)),
      tap(() => this.requestUpdate())
    );
    backend$.subscribe();
  }

  echo() {
    const input = "Hola Mundo";
    const result$ = this.poteSrv
      .echo$(input)
      .pipe(tap((info) => console.log("GOTICA de ECHO:", info)));
    result$.subscribe();
  }

  login() {
    const user = "peter";
    const password = "1234";
    const result$ = this.poteSrv
      .login$(user, password)
      .pipe(tap((info) => console.log("Usuario correcto?", info)));
    result$.subscribe();
  }

  configuration() {
    const configuration = {
      branch: "Feria del Centro",
      rate: {
        usd: { destination: "ved", rate: 5 },
        ved: { destination: "usd", rate: 0.2 },
      },
    };
    const result$ = this.poteSrv
      .setup$(configuration)
      .pipe(tap((info) => console.log("Configuración:", info)));
    result$.subscribe();
  }

  openPos() {
    const validData = {
      brahch: "Feria del Centro",
      initialCash: 100,
      departament: "Mascotas",
    };

    let { branch, initialCash, departament } = validData;

    const result$ = this.poteSrv
      .openPos$(initialCash, branch, departament)
      .pipe(
        tap((info) => console.log("Abrió la caja?", info)),
        tap((result) =>
          result === 200
            ? (this.message = "Caja Abierta")
            : (this.message = `Error abriendo caja (${result})`)
        ),
        catchError(
          (error) =>
            (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
        ),
        tap(() => this.requestUpdate())
      );
    result$.subscribe();
  }

  createInvoice() {
    const cqc = "Sarah Connors";
    const result$ = this.poteSrv.createInvoice$(cqc).pipe(
      tap((info) => console.log("Factura nueva?", info)),
      catchError(
        (error) =>
          (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
      ),
      tap(() => this.requestUpdate())
    );
    result$.subscribe();
  }

  createInvoiceShortcut() {
    this.login();
    this.configuration();
    this.openPos();
    this.createInvoice();
  }

  findPrice() {
    const barcode = "103";
    const result$ = this.poteSrv.findPrice$(barcode).pipe(
      tap((info) => console.log("Precio encontrado?", info)),
      tap(
        (price) =>
          (this.message = `Precio de ${price.name} es: ${price.price.ved}Bs  :  ${price.price.usd}USD`)
      ),
      catchError(
        (error) =>
          (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
      ),
      tap(() => this.requestUpdate())
    );
    result$.subscribe();
  }

  addItem() {
    const barcode = "103";
    const quantity = 3;
    const result$ = this.poteSrv.findPrice$(barcode).pipe(
      tap((info) => console.log("resultado de findPrice?", info)),
      map((price) => ({ price, quantity })),
      /*
       * Equivalente en promesas
       * No son exactmaente iguales, pero la intuición es la misma
       * Espera que se ejecute una promesa y con el resultado ejecuto la otra
       * this.poteSrv.findPrice().then(price => this.poteSrv.addItem(price))
       */
      switchMap((item) => this.poteSrv.addItem$(item)),
      tap((info) => console.log("add Product?", info)),
      tap((price) => (this.message = `Producto Agregado`)),
      catchError(
        (error) =>
          (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
      ),
      tap(() => this.requestUpdate())
    );
    result$.subscribe();
  }

  saveDraftInvoice() {
    const result$ = this.poteSrv.saveDraftInvoice$().pipe(
      tap((info) => console.log("Factura guardada?", info)),
      tap((invoice) => (this.message = `Factura guardada:` + invoice.id)),
      catchError(
        (error) =>
          (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
      ),
      tap(() => this.requestUpdate())
    );
    result$.subscribe();
  }

  makePayment(amount, currency) {
    const payment = {
      amount,
      currency,
    };
    const result$ = this.poteSrv.makePayment$(payment).pipe(
      tap((info) => console.log("Factura guardada?", info)),
      tap((invoice) => (this.message = `Factura guardada:` + invoice.id)),
      catchError(
        (error) =>
          (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
      ),
      tap(() => this.requestUpdate())
    );
    result$.subscribe();
  }

  makePaymentVed() {
    this.makePayment(20, "ved");
  }

  makePaymentUsd() {
    this.makePayment(5, "usd");
  }


//============================================================
  removeItem(){
        
    const result$ = this.poteSrv.removeItem$()
        .pipe(
            tap(invoice =>console.log("invoice",invoice)),
            catchError(error => this.message = ` ${error.message} `),
            tap( () => this.requestUpdate())
            
           

        )
        
        result$.subscribe()

}

//=============================================================

//=====================================================

clearInvoice() {
    const result$ = this.poteSrv.clearInvoice$().pipe(
      tap((info) => console.log("INVOICE STATUS: ", info)),
      tap((this.message = `Factura Removida` )),
      catchError(
        (error) =>
          (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
      ),
      tap(() => this.requestUpdate())
    );
    result$.subscribe();
  }

// ====================================================

  recoverDraftInvoice() {
    const id = 1;
    const result$ = this.poteSrv.recoverDraftInvoice$(id).pipe(
      tap((info) => console.log("Factura recuperada?", info)),
      tap((invoice) => (this.message = `Factura recuperada:` + invoice.id)),
      catchError(
        (error) =>
          (this.message = `Sucedio un error: ${error.message} - ${error.code}`)
      ),
      tap(() => this.requestUpdate())
    );
    result$.subscribe();
  }

  turn

  returnPayment(){
    
  }

}

customElements.define("ceco-main", MainComponent);
